﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTaskLibrary.Interface
{
    public interface  IFigure
    {
        double GetSize();
    }
}
