﻿using TestTaskLibrary.Interface;

namespace TestTaskLibrary.Utils
{
    public static class Utils
    {
        public static double CalculateFigureSize(IFigure figure)
        {
            return figure.GetSize();
        }
    }
}
