﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTaskLibrary.Interface;

namespace TestTaskLibrary.Figures
{
    public sealed class Triangle: IFigure
    {
        private double A;
        private double B;
        private double C;
        public Triangle (double A,double B,double C)
        {
            this.A = A;
            this.B = B;
            this.C = C;
        }

        public double GetSize()
        {
            //использую формулу герона для вычисления площади треугольника
            var P = (A + B + C) / 2;
            return Math.Sqrt(P * (P - A) * (P - B) * (P - C));
        }

        public bool IsPerp()
        {
            var param = new List<double>() { A, B, C };
            double _maxSize = 0;
            double _sizeA = 0;
            double _sizeB = 0;
            if (param.Count > 3) return false;
            param.Sort();
            _maxSize = param[0];
            _sizeA = param[1];
            _sizeB = param[2];
            if (Math.Pow(_maxSize, 2) == Math.Pow(_sizeA, 2) + Math.Pow(_sizeB, 2))
            {
                return true;
            }
            return false;
             
        }
    }
}
