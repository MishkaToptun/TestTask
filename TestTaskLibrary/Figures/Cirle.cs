﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTaskLibrary.Interface;

namespace TestTaskLibrary.Figures
{
    public sealed class Cirle : IFigure
    {
        private double _radius;
        public Cirle(double r)
        {
            _radius = r;
        }

        public double GetSize()
        {
            return Math.PI * Math.Pow(_radius, 2);
        }
    }
}
