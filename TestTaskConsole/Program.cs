﻿using System;
using System.Collections.Generic;
using System.Linq;

using TestTaskLibrary.Figures;
using TestTaskLibrary.Utils;
//для примера можно выводить площадь так. либо использовать статичную функцию
using TestTaskLibrary.Interface;

namespace TestTaskConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var circle = new Cirle(2);
            var triangle = new Triangle(5.66, 3, 4);
            //можно сделать вывод площади так
            Console.WriteLine(Utils.CalculateFigureSize(circle));
            Console.WriteLine(Utils.CalculateFigureSize(triangle));
            //либо так
            PrintFigureSize(circle);
            Console.WriteLine(triangle.IsPerp());
            Console.ReadKey();
        }
        static void PrintFigureSize(IFigure figure)
        {
            Console.WriteLine(figure.GetSize());
        }
    }
}
